{**
 * plugins/blocks/credits/block.tpl
 *
 * Copyright (c) 2018 Roberto Salas
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common site sidebar menu -- "Credits" block.
 *}
<div class="pkp_block block_credit">
	<div class="content">
		<a href="http://www.uautonoma.cl/">
			<img src="/plugins/blocks/credits/logo_ua.png"></img>
		</a>
	</div>
</div>
