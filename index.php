<?php

/**
 * @defgroup plugins_blocks_credits credits block plugin
 */

/**
 * @file plugins/blocks/credits/index.php
 *
 * Copyright (c) 2018 Roberto Salas
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_blocks_credits
 * @brief Wrapper for "credits" block plugin.
 *
 */

require_once('CreditsBlockPlugin.inc.php');

return new CreditsBlockPlugin();

?>
